export default class UrlParser {
  // parser.protocol; // => "http:"
  // parser.host;     // => "example.com:3000"
  // parser.hostname; // => "example.com"
  // parser.port;     // => "3000"
  // parser.pathname; // => "/pathname/"
  // parser.hash;     // => "#hash"
  // parser.search;   // => "?search=test"
  // parser.origin;   // => "http://example.com:3000"

  constructor(string) {
    const parser = document.createElement('a');
    parser.href = string;
    this.parser = parser;
  }

  getInstanceKey() {
    return `${this.parser.origin}/`;
  }

  getAllData() {
    const instance = this.getInstanceKey();
    const result = this.parser.pathname.match(/\/([\w-/.]+)issues\/(\d+)/);
    if (result instanceof Array) {
      const [, pathString, issue] = result;
      const path = pathString.split('/').slice(0, -1);

      return { instance, path, issue };
    }
    return {
      instance,
      path: null,
      issue: null,
    };
  }

  getPath() {
    return this.getAllData().path;
  }

  getIssueNr() {
    return this.getAllData().issue;
  }
}
